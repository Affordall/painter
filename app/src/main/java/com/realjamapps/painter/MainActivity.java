package com.realjamapps.painter;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.UUID;

import petrov.kristiyan.colorpicker.ColorPicker;


public class MainActivity extends AppCompatActivity {

    private float smallBrush, mediumBrush, largeBrush;

    private DView canvas = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                handleDrawingIconTouched(item.getItemId());
                return false;
            }
        });

        smallBrush = getResources().getInteger(R.integer.small_size);
        mediumBrush = getResources().getInteger(R.integer.medium_size);
        largeBrush = getResources().getInteger(R.integer.large_size);

        canvas = (DView)this.findViewById(R.id.custom_draw_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current draw state
        savedInstanceState.putParcelable("saved_image", canvas.getmBitmap());
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can restore the view hierarchy
        super.onRestoreInstanceState(savedInstanceState);
        Bitmap tempBmp = savedInstanceState.getParcelable("saved_image");
        assert tempBmp != null;
        Bitmap mutableBitmap = tempBmp.copy(Bitmap.Config.ARGB_8888, true);
        canvas.drawBitmap(mutableBitmap);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }


    /**
     * Switch statement for pick desire action by clicking on Menu Items in Toolbar.
     * @param itemId int resource
     * @return void
     */
    private void handleDrawingIconTouched(int itemId) {
        switch (itemId) {
            case R.id.action_delete:
                deleteDialog();
                break;

            case R.id.action_erase:
                pickEraser();
                break;

            case R.id.action_brush:
                pickBrush();
                break;

            case R.id.action_save:
                savePicture();
                break;

            case R.id.action_figure:
                pickFigure();
                break;

            case R.id.action_color:
                pickupColor();
                break;
        }
    }

    /**
     * This method create Alert.Dialog and call canvas.clear for delete current drawings.
     * @return void
     */
    private void deleteDialog() {
        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(this);
        deleteDialog.setTitle(getString(R.string.delete_drawing_title));
        deleteDialog.setMessage(getString(R.string.delete_drawing_warning));
        deleteDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                canvas.clear();
                // Just to be sure
                System.gc();
                dialog.dismiss();
            }
        });
        deleteDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        deleteDialog.show();
    }

    private void savePicture() {
        AlertDialog.Builder saveDialog = new AlertDialog.Builder(this);
        saveDialog.setTitle("Save drawing");
        saveDialog.setMessage("Save drawing to Gallery?");
        saveDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                canvas.setDrawingCacheEnabled(true);
                String imgSaved = MediaStore.Images.Media.insertImage(
                        getContentResolver(), canvas.getDrawingCache(),
                        UUID.randomUUID().toString() + ".png", "drawing");
                if (imgSaved != null) {
                    Toast savedToast = Toast.makeText(getApplicationContext(),
                            "Drawing saved to Gallery!", Toast.LENGTH_SHORT);
                    savedToast.show();
                } else {
                    Toast unsavedToast = Toast.makeText(getApplicationContext(),
                            "Oops! Image could not be saved.", Toast.LENGTH_SHORT);
                    unsavedToast.show();
                }
                canvas.destroyDrawingCache();
            }
        });
        saveDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        saveDialog.show();
    }


    private void pickupColor() {
        final ColorPicker colorPicker = new ColorPicker(MainActivity.this);
        if (isTablet(getApplicationContext())) {
            colorPicker.setDialogInTabletMode();
        }
        colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
            @Override
            public void setOnFastChooseColorListener(int position, int color) {
                canvas.setPaintStrokeColor(color);
                colorPicker.dismissDialog();
            }
        }).setDefaultColor(Color.parseColor("#f84c44")).setColumns(5).setRoundButton(true).show();
    }

    private void pickBrush() {
        final Dialog brushDialog = new Dialog(this);
        brushDialog.setTitle("Brush size:");
        brushDialog.setContentView(R.layout.brush_and_erase_chooser);
        canvas.setMode(DView.Mode.DRAW);
        canvas.setDrawer(DView.Drawer.PEN);
        canvas.setPaintStyle(Paint.Style.STROKE);

        ImageButton smallBtn = (ImageButton)brushDialog.findViewById(R.id.small_brush);
        smallBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas.setPaintStrokeWidth(smallBrush);
                brushDialog.dismiss();
            }
        });
        ImageButton mediumBtn = (ImageButton)brushDialog.findViewById(R.id.medium_brush);
        mediumBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas.setPaintStrokeWidth(mediumBrush);
                brushDialog.dismiss();
            }
        });

        ImageButton largeBtn = (ImageButton)brushDialog.findViewById(R.id.large_brush);
        largeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas.setPaintStrokeWidth(largeBrush);
                brushDialog.dismiss();
            }
        });

        brushDialog.show();
    }

    private void pickEraser() {
        /** done */
        int mrWhite = Color.WHITE;
        canvas.setMode(DView.Mode.ERASER);
        canvas.setPaintStrokeColor(mrWhite);
        canvas.setPaintFillColor(mrWhite);
    }

   private void pickFigure() {
        final Dialog figureDialog = new Dialog(this);
       figureDialog.setTitle("Figure:");
       figureDialog.setContentView(R.layout.figure_chooser);
       canvas.setPaintStyle(Paint.Style.FILL);
        ImageButton figRect = (ImageButton) figureDialog.findViewById(R.id.fig_rect);
        figRect.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas.setDrawer(DView.Drawer.RECTANGLE);
                figureDialog.dismiss();
            }
        });
        ImageButton figCircle = (ImageButton) figureDialog.findViewById(R.id.fig_circle);
        figCircle.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas.setDrawer(DView.Drawer.CIRCLE);
                figureDialog.dismiss();
            }
        });

        ImageButton figLine = (ImageButton) figureDialog.findViewById(R.id.fig_line);
       figLine.setOnClickListener(new OnClickListener() {
           @Override
           public void onClick(View v) {
               canvas.setDrawer(DView.Drawer.LINE);
               canvas.setPaintStyle(Paint.Style.STROKE);
               figureDialog.dismiss();
           }
       });

        figureDialog.show();
    }

    private static boolean isTablet(Context context) {
        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);

        return (xlarge || large);
    }

}
