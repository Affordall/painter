package com.realjamapps.painter;

import java.util.List;
import java.util.ArrayList;
import android.graphics.PorterDuff;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MotionEvent;
import android.content.Context;
import android.graphics.Path;
import android.graphics.PorterDuffXfermode;

public class DView extends View {

    // Drawer
    public enum Drawer {
        PEN,
        LINE,
        RECTANGLE,
        CIRCLE
    }

    // Mode
    public enum Mode {
        DRAW,
        ERASER
    }

    private Context context = null;
    private Canvas mCanvas = null;
    private Bitmap mBitmap = null;

    private List<Path> pathStorage = new ArrayList<>();
    private List<Paint> paintsStorage = new ArrayList<>();

    // Drawer
    private float startX   = 0F;
    private float startY   = 0F;
    //private float controlX = 0F;
    //private float controlY = 0F;

    // Undo
    private int historyPointer = 0;

    // Flags
    private Mode mode      = Mode.DRAW;
    private Drawer drawer  = Drawer.PEN;
    private boolean isDown = false;

    // Paint
    private Paint.Style paintStyle = Paint.Style.STROKE;
    private int paintStrokeColor   = Color.BLACK;
    private int paintFillColor     = Color.WHITE;
    private float paintStrokeWidth = 10;
    private Paint.Cap lineCap      = Paint.Cap.ROUND;



    public DView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.init(context);
    }

    public DView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.init(context);
    }

    public DView(Context context) {
        super(context);
        this.init(context);
    }

    private void init(Context context) {
        this.context = context;

        this.pathStorage.add(new Path());
        this.paintsStorage.add(this.createPaint());
        this.historyPointer++;
    }

    private Paint createPaint() {
        Paint paint = new Paint();

        paint.setAntiAlias(true);
        paint.setStyle(this.paintStyle);
        paint.setStrokeWidth(this.paintStrokeWidth);
        paint.setStrokeCap(this.lineCap);
        paint.setStrokeJoin(Paint.Join.MITER);

        if (this.mode == Mode.ERASER) {
            // Eraser
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            //paint.setARGB(0, 0, 0, 0);

            //paint.setColor(this.baseColor);
        } else {
            // Other
            paint.setColor(this.paintStrokeColor);
        }

        return paint;
    }

    private Path createPath(MotionEvent event) {
        Path path = new Path();

        // Save for ACTION_MOVE
        this.startX = event.getX();
        this.startY = event.getY();

        path.moveTo(this.startX, this.startY);

        return path;
    }

    private void updateHistory(Path path) {
        if (this.historyPointer == this.pathStorage.size()) {
            this.pathStorage.add(path);
            this.paintsStorage.add(this.createPaint());
            this.historyPointer++;
        } else {
            // On the way of Undo or Redo
            this.pathStorage.set(this.historyPointer, path);
            this.paintsStorage.set(this.historyPointer, this.createPaint());
            this.historyPointer++;

            for (int i = this.historyPointer, size = this.paintsStorage.size(); i < size; i++) {
                this.pathStorage.remove(this.historyPointer);
                this.paintsStorage.remove(this.historyPointer);
            }
        }
    }

    private Path getCurrentPath() {
        return this.pathStorage.get(this.historyPointer - 1);
    }

    private void onActionDown(MotionEvent event) {
        switch (this.mode) {
            case DRAW   :
            case ERASER :
                    this.updateHistory(this.createPath(event));
                    this.isDown = true;
                break;
            default :
                break;
        }
    }

    private void onActionMove(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        switch (this.mode) {
            case DRAW   :
            case ERASER :

                    if (!isDown) {
                        return;
                    }

                    Path path = this.getCurrentPath();

                    switch (this.drawer) {
                        case PEN :
                            path.lineTo(x, y);
                            break;
                        case LINE :
                            path.reset();
                            path.moveTo(this.startX, this.startY);
                            path.lineTo(x, y);
                            break;
                        case RECTANGLE :
                            path.reset();
                            path.addRect(this.startX, this.startY, x, y, Path.Direction.CCW);
                            break;
                        case CIRCLE :
                            //double distanceX = Math.abs((double) (this.startX-x));
                            //double distanceY = Math.abs((double)(this.startX-y));
                            //double radius = Math.sqrt(Math.pow(distanceX, 2.0) + Math.pow(distanceY, 2.0)) / 2;
                            double radius = calculateRadius(this.startX,this.startY,x,y);

                            path.reset();
                            path.addCircle(this.startX, this.startY, (float)radius, Path.Direction.CCW);
                            break;
                        default :
                            break;
                    }

                break;
            default :
                break;
        }
    }

    private void onActionUp(MotionEvent event) {
        if (isDown) {
            this.startX = 0F;
            this.startY = 0F;
            this.isDown = false;
        }
    }

    /**
     * Method which calculate Radius of Circle over start and finish positions.
     * @param x1 - Start X
     * @param y1 - Start Y
     * @param x2 - Finish X
     * @param y2 - Finish Y
     * @return radius in Float
     */
    protected float calculateRadius(float x1, float y1, float x2, float y2) {

        return (float) Math.sqrt(
                Math.pow(x1 - x2, 2) +
                        Math.pow(y1 - y2, 2)
        );
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Before "drawPath"
        int baseColor = Color.WHITE;
        canvas.drawColor(baseColor);

        if (this.mBitmap != null) {
            canvas.drawBitmap(this.mBitmap, 0F, 0F, new Paint());
        }

        for (int i = 0; i < this.historyPointer; i++) {
            Path path   = this.pathStorage.get(i);
            Paint paint = this.paintsStorage.get(i);

            canvas.drawPath(path, paint);
        }

        this.mCanvas = canvas;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                this.onActionDown(event);
                break;
            case MotionEvent.ACTION_MOVE :
                this.onActionMove(event);
                break;
            case MotionEvent.ACTION_UP :
                this.onActionUp(event);
                break;
            default :
                break;
        }

        this.invalidate();

        return true;
    }

    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public void setDrawer(Drawer drawer) {
        this.drawer = drawer;
    }

    public void clear() {
        Path path = new Path();
        path.moveTo(0F, 0F);
        path.addRect(0F, 0F, getWidth(), getHeight(), Path.Direction.CCW);
        path.close();

        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);

        if (this.historyPointer == this.pathStorage.size()) {
            this.pathStorage.add(path);
            this.paintsStorage.add(paint);
            this.historyPointer++;
        } else {

            this.pathStorage.set(this.historyPointer, path);
            this.paintsStorage.set(this.historyPointer, paint);
            this.historyPointer++;

            for (int i = this.historyPointer, size = this.paintsStorage.size(); i < size; i++) {
                this.pathStorage.remove(this.historyPointer);
                this.paintsStorage.remove(this.historyPointer);
            }
        }

        this.invalidate();
    }

    public void setPaintStyle(Paint.Style style) {
        this.paintStyle = style;
    }

    public void setPaintStrokeColor(int color) {
        this.paintStrokeColor = color;
    }

    public void setPaintFillColor(int color) {
        this.paintFillColor = color;
    }

    public void setPaintStrokeWidth(float width) {
        if (width >= 0) {
            this.paintStrokeWidth = width;
        } else {
            this.paintStrokeWidth = 10;
        }
    }

    public Bitmap getmBitmap() {
        this.setDrawingCacheEnabled(false);
        this.setDrawingCacheEnabled(true);

        return Bitmap.createBitmap(this.getDrawingCache());
    }

    public void drawBitmap(Bitmap bitmap) {
        this.mBitmap = bitmap;
        this.invalidate();
    }
}
